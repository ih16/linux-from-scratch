#!/bin/bash
export LFS=/mnt/lfs
echo $LFS
sleep 3
mount -v -t ext4 /dev/sdb2 $LFS
mount -v -t ext2 /dev/sdb1 $LFS/boot
mount -v -t ext4 /dev/sdb3 $LFS/home
mount -v -t ext4 /dev/sdb4 $LFS/usr
mount -v -t ext4 /dev/sdb5 $LFS/tmp
mount -v -t ext4 /dev/sdb5 $LFS/opt
mount -v -t ext4 /dev/sdb7 $LFS/usr/src
/sbin/swapon -v /dev/sdb8
